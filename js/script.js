/*Alert message for social media */
function fb() {
  alert("Sign up Facebook");
}
function tw() {
  alert("Sign up Twitter ");
}
function insta() {
  alert("Sign up Instagram");
}
/*Email validation */
function validation()
{
  var form= document.getElementById("myForm");
  var email= document.getElementById("email").value;
  var info= document.getElementById("info");
  var pattern= /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

  if (email.match(pattern))
  {
    form.classList.add("Valid");
    form.classList.remove("Invalid");
    info.innerHTML="Valid Email Address";
    info.style.color= "green";
  }
  else
  {
    form.classList.add("Invalid");
    form.classList.remove("Valid");
    info.innerHTML="Invalid Email Address";
    info.style.color= "red";
  }
}
/*function for button*/
function subscription() {
  alert("Thank you for subscription!!");
  document.myForm.reset();
}